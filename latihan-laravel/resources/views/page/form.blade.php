<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
    <label for="firstname">First Name : </label><br>
    <input type="text" name="firstname"><br>
    <label for="lastname">Last Name : </label><br>
    <input type="text" name="lastname"><br><br>
    <label>Gender : </label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label for="nationality">Nationality:</label>
        <select name="nationality" id="nationality">
          <option value="1">Indonesian</option>
          <option value="2">Malaysian</option>
          <option value="3">Singapura</option>
          <option value="4">Thailand</option>
          <option value="5">Kamboja</option>
        </select>
        <br /><br />
        <label>Language Spoken</label>
        <br /><br />
  
        <input type="checkbox" name="language[]" value="Bahasa Indonesia"> Bahasa Indonesia<br>
        <input type="checkbox" name="language[]" value="English"> English<br>
        <input type="checkbox" name="language[]" value="Arabic"> Arabic<br> 
        <input type="checkbox" name="language[]" value="Japaese"> Japanese<br> <br> 
        <label >Bio</label><br>
    <textarea name="bio"cols="30" rows="10"></textarea><br><br> 
    <input type="submit" value="Sign Up">
    </form>
</body>
</html>