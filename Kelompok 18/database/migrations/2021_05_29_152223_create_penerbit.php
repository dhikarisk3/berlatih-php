<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenerbit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penerbit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_penerbit');
            $table->string('lokasi_penerbit');
        });
        DB::table('penerbit')->insert([
            ['nama_penerbit' => 'Gramedia', 'lokasi_penerbit' => 'Semarang'],
            ['nama_penerbit' => 'Pustaka Ilmu', 'lokasi_penerbit' => 'Solo'],
            ['nama_penerbit' => 'Cahaya Dunia', 'lokasi_penerbit' => 'Yogyakarta']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerbit');
    }
}