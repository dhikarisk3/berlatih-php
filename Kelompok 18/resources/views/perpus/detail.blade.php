@extends('layouts.master')
@section('title')
Data Buku : <b> {{$perpus->judul}}</b>
@endsection
@section('content')
 <div class="row">
    <div class="col-lg-12">
      <div class="card mb-3">
        <div class="row no-gutters">
          <div class="col-md-8">
            <div class="card-body">
              <table class="col-md-6 table-condensed">
                <tr>
                  <th>Judul</th>
                  <td></td>
                  <td>{{$perpus->judul}}</td>
                </tr>
                <tr>
                  <th>ISBN</th>
                  <td></td>
                  <td>{{$perpus->ISBN}}</td>
                </tr>
                <tr>
                  <th>No Induk</th>
                  <td></td>
                  <td>{{$perpus->no_induk}}</td>
                </tr>
                <tr>
                  <th>Nama Pengarang</th>
                  <td></td>
                  <td>{{$perpus->nama_pengarang}}</td>
                </tr>
                <tr>
                  <th>Nama Penerbit</th>
                  <td></td>
                  <td>{{$perpus->nama_penerbit}}</td>
                </tr>
              </table>
              <br>
              <a href="/perpus" class="btn btn-info">Kembali</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

