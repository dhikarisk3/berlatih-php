@extends('layouts.master')
@section('title')
Edit Data Buku : <b> {{$perpus->judul}}</b>
@endsection
@section('content')
<div class="card">
    <div class="card-header">
       <a href="/perpus" class="btn btn-info">Kembali</a>
    </div>
    <div class="card-body">
        
        {{-- Content --}}
        <div>
            <form action="/perpus/{{$perpus->id}}" method="POST">
                @csrf
                @method('PUT')
                    <div class="form-group">
                        <label for="title">Judul Buku</label>
                        <input type="text" class="form-control" name="judul" value="{{$perpus->judul}}" id="judul" placeholder="Masukkan Judul Buku">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">ISBN</label>
                        <input type="text" class="form-control" name="isbn" value="{{$perpus->ISBN}}" id="isbn" placeholder="Masukkan ISBN">
                        @error('isbn')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Nomor Index</label>
                        <input type="text" class="form-control" name="no_induk" value="{{$perpus->no_induk}}" id="no_induk" placeholder="Masukkan Induk Buku">
                        @error('no_induk')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Pilih Pengarang</label>
                        <select name="pengarang_id" id="pengarang_id" class="form-control">
                            @foreach ($nama_pengarang as $key=>$value)
                                <option value="{{$value->id}}">{{$value->nama_pengarang}}</option>
                            @endforeach
                        </select>
                        @error('pengarang_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Pilih Penerbit</label>
                        <select name="penerbit_id" id="penerbit_id" class="form-control">
                            @foreach ($nama_penerbit as $key=>$value)
                                <option value="{{$value->id}}">{{$value->nama_penerbit}}</option>
                            @endforeach
                        </select>
                        @error('penerbit_id')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Edit</button>
                    
                </form>
        </div>
    </div>
<!-- /.card-body -->
</div>

@endsection
