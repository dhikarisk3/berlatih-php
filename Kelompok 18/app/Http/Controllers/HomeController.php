<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $perpus = DB::table('buku')
            ->join('penerbit', 'buku.penerbit_id', '=', 'penerbit.id')
            ->join('pengarang', 'buku.pengarang_id', '=', 'pengarang.id')->paginate(5);
        return view('FrontEnd.index', compact('perpus'));
    }
    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table pegawai sesuai pencarian data
        $perpus = DB::table('buku')
            ->join('penerbit', 'buku.penerbit_id', '=', 'penerbit.id')
            ->join('pengarang', 'buku.pengarang_id', '=', 'pengarang.id')
            ->where('judul', 'like', "%" . $cari . "%")
            ->paginate(500);

        // mengirim data pegawai ke view index
        return view('FrontEnd.index', ['perpus' => $perpus]);
    }
}