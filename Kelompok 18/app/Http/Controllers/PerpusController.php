<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PerpusController extends Controller
{
    public function index()
    {
        $perpus = DB::table('buku')->get();
        return view('perpus.dashboard', compact('perpus'));
    }
    public function create()
    {
        $nama_pengarang = DB::table('pengarang')->get();
        $nama_penerbit = DB::table('penerbit')->get();
        return view('perpus.create', compact('nama_penerbit', 'nama_pengarang'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:buku',
            'isbn' => 'required',
            'no_induk' => 'required',
            'pengarang_id' => 'required',
            'penerbit_id' => 'required',
        ]);
        $query = DB::table('buku')->insert([
            "judul" => $request["judul"],
            "isbn" => $request["isbn"],
            "no_induk" => $request["no_induk"],
            "pengarang_id" => $request["pengarang_id"],
            "penerbit_id" => $request["penerbit_id"]
        ]);
        return redirect('/perpus');
    }
    public function show()
    {
        $perpus = DB::table('buku')
            ->join('penerbit', 'buku.penerbit_id', '=', 'penerbit.id')
            ->join('pengarang', 'buku.pengarang_id', '=', 'pengarang.id')
            ->select('buku.*', 'penerbit.nama_penerbit', 'pengarang.nama_pengarang')
            ->paginate(5);
        return view('perpus.show', compact('perpus'));
    }
    public function detail($id)
    {
        $perpus = DB::table('buku')
            ->join('penerbit', 'buku.penerbit_id', '=', 'penerbit.id')
            ->join('pengarang', 'buku.pengarang_id', '=', 'pengarang.id')
            ->select('buku.*', 'penerbit.nama_penerbit', 'pengarang.nama_pengarang')
            ->where('buku.id', $id)
            ->first();
        return view('perpus.detail', compact('perpus'));
    }
    public function edit($id)
    {
        $nama_pengarang = DB::table('pengarang')->get();
        $nama_penerbit = DB::table('penerbit')->get();
        $perpus = DB::table('buku')->where('id', $id)->first();
        return view('perpus.edit', compact('perpus', 'nama_penerbit', 'nama_pengarang'));
    }
    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:buku',
            'isbn' => 'required',
            'no_induk' => 'required',
            'pengarang_id' => 'required',
            'penerbit_id' => 'required',
        ]);

        $query = DB::table('buku')
            ->where('buku.id', $id)
            ->update([
                "judul" => $request["judul"],
                "isbn" => $request["isbn"],
                "no_induk" => $request["no_induk"],
                "pengarang_id" => $request["pengarang_id"],
                "penerbit_id" => $request["penerbit_id"]
            ]);
        return redirect('/perpus');
    }
    public function cari(Request $request)
    {
        // menangkap data pencarian
        $cari = $request->cari;

        // mengambil data dari table pegawai sesuai pencarian data
        $perpus = DB::table('buku')
            ->join('penerbit', 'buku.penerbit_id', '=', 'penerbit.id')
            ->join('pengarang', 'buku.pengarang_id', '=', 'pengarang.id')
            ->where('judul', 'like', "%" . $cari . "%")
            ->paginate(500);

        // mengirim data pegawai ke view index
        return view('perpus.show', ['perpus' => $perpus]);
    }
    public function destroy($id)
    {
        $query = DB::table('buku')->where('id', $id)->delete();
        return redirect('/perpus');
    }
}