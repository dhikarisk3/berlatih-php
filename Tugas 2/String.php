<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        $kalimat1 = "Hello PHP!" ;
        echo "<p>$kalimat1</p>";
        $panjang = strlen($kalimat1);
        $jumlahkata = str_word_count($kalimat1);
        echo "<p>Panjang String = $panjang</p>";
        echo "<p>Jumlah Kata = $jumlahkata</p>";
        "</br>";
        $kalimat2 = "I'm ready for the challenges" ;
        echo "<p>$kalimat2</p>";
        $panjang1 = strlen($kalimat2);
        $jumlahkata1 = str_word_count($kalimat2);
        echo "<p>Panjang String = $panjang1</p>";
        echo "<p>Jumlah Kata = $jumlahkata1</p>";
        

        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        
        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $string2 = "I love PHP";
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " . substr($string2, 2, 4)  ;
        echo "<br> Kata Ketiga: " . substr($string2, 7, 3)  ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\" <br>"; 
        // OUTPUT : "PHP is old but awesome"
        echo "<label>" .str_replace("sexy!","awesome",$string3). "</label>";

    ?>
</body>
</html>